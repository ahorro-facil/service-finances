package com.ahorro.facil.finance.logic.repositories;

import java.util.List;

import com.ahorro.facil.finance.logic.model.Incomes;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IncomeRepository extends JpaRepository<Incomes, Long>{
    @Query(
        value = "SELECT * FROM incomes i WHERE i.user_id = ?1",
        nativeQuery = true
    )
    List<Incomes> findByUserId(long userId);
}
