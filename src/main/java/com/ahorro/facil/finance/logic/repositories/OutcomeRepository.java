package com.ahorro.facil.finance.logic.repositories;

import com.ahorro.facil.finance.logic.model.Outcomes;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OutcomeRepository extends JpaRepository<Outcomes, Long>{
    
}
