package com.ahorro.facil.finance.logic.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "incomes")
public class Incomes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "active")
	private boolean active;

    public Incomes(String title, Double amount, long userId, boolean active) {
        this.title = title;
        this.amount = amount;
        this.userId = userId;
        this.active = active;
    }

}
