package com.ahorro.facil.finance.logic.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "outcomes")
public class Outcomes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "user_id")
    private int user_id;

    @Column(name = "active")
	private boolean active;

    public Outcomes(String title, Double amount, int user_id, boolean active) {
        this.title = title;
        this.amount = amount;
        this.user_id = user_id;
        this.active = active;
    }
   
}
