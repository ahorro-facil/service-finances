package com.ahorro.facil.finance.logic.controllers;

import java.util.List;

import com.ahorro.facil.finance.logic.model.Incomes;
import com.ahorro.facil.finance.logic.repositories.IncomeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/finances")
public class IncomeController {

    @Autowired
    IncomeRepository incomeRepository;


    @GetMapping("/incomes")
    public ResponseEntity<List<Incomes>> findIncomesByUserId(@RequestParam(required = true) long id) {
        List<Incomes> incomes = incomeRepository.findByUserId(id);
        return new ResponseEntity<>(incomes, HttpStatus.OK);
    }

    @PostMapping("/incomes")
    public ResponseEntity<Incomes> registerIncome(@RequestBody Incomes incomes) {
        try {
            Incomes created = incomeRepository.save(
                    new Incomes(incomes.getTitle(), incomes.getAmount(), incomes.getUserId(), incomes.isActive()));
            return new ResponseEntity<>(created, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
