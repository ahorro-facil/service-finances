package com.ahorro.facil.finance.logic;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/finances")
public class MainController {
    @GetMapping
    public String saludo(){
        return "Hello from finances";
    }
}
